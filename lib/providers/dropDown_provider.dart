
import 'package:flutter/foundation.dart';

class Dropdown_provider with ChangeNotifier{
  String _centerText = 'Tap the dropdown';

  String get centerText{
    return _centerText;
  }


  void changeText(String value) {
    _centerText = value;
    notifyListeners();
  }

}