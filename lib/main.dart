import 'package:dropdown_widget/textWidget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'providers/dropDown_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => Dropdown_provider(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

List<String> dropdownList = [];

class _MyHomePageState extends State<MyHomePage> {

  String dropDownValue = '1';

  @override
  Widget build(BuildContext context) {
    Dropdown_provider dropdownProvider = Provider.of<Dropdown_provider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Dropdown widget'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(),
            TextWidget(),
            Container(
              child: Container(
                alignment: Alignment.center,
                width: 200,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.deepPurple, width: 2)
                ),
                child: DropdownButton<String>(
                  dropdownColor: Colors.grey,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                  value: dropDownValue,
                  onChanged: (String newValue) {
                    setState(() {
                      dropDownValue = newValue;
                    });
                    dropdownProvider.changeText(dropDownValue);
                  },
                  elevation: 10,
                  items: List<String>.generate(100, (index) => (index + 1).toString())
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      child: Text(value),
                      value: value,
                    );
                  }).toList(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
