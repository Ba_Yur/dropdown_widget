import 'package:dropdown_widget/providers/dropDown_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String text = Provider.of<Dropdown_provider>(context).centerText;
    return Text(
      text,
      style: TextStyle(
        fontSize: 50,
      ),
    );
  }
}
